# RWDS dChan specs
> Specifications for the RWDS decentralised image board


## Background
**Current image boards**&emsp;
Currently, image boards exist almost exclusively in the form of websites which allow their users to post text, images, videos, and other files quasi-anonymously (with a temporary identity).
Thorough content moderation on such an image board is important to protect the site operator from accusations of spreading illegal content.
Moderators, lovingly called janitors or "jannies", are individuals who can delete unlawful posts, threads, or ban posters.
However, most often, moderators have an additional political agendas to shut down discussion amongst dissidents whom they don't agree with politically.
In other cases, image board websites have been shut down by their providers over controversial content posted by users.

## Goals of the project
This project aims to create a completely decentralised and anonymous image board that will not have a central point of failure (website operator), and be resistant to censorship ("elected" moderators).

**Decentralization**&emsp;
Decentralisation makes the network practically owner-less, and thus, no single individual can be persecuted by the law for operating it.
There is no specific set of individuals that can be compromised to take control of the network and censor it.

**Redundancy**&emsp;
Since the image board is operated collectively by many participants, partial network outages will not damage the system as a whole.
Targeting any subset of co-operators will not result in any harm to the system.

**Anonymity**&emsp;
The image board is completely anonymous, and no connection to real-world identities can be gathered by external parties, unless they have complete control over the whole network infrastructure.
Users can freely and without fear of persecution speak their minds.

**Low entry barrier**&emsp;
The hardware requirements for participating in the network should be low enough to make it affordable for basically everyone to own a device that can co-operate the image board 24/7.
The hardware cost, as well as electricity consumption should be kept as low as reasonably possible (if not in conflict with other goals).

**Authenticity**&emsp;
It should be impossible to fake, edit and/or delete another user's messages over the network.


## Architecture

**Peers/nodes**&emsp;
Peers or nodes are users/their devices that are connected to the image board and co-operate it.
Together, all nodes form the whole network.

**IDs**&emsp;
Posters need to create cryptographic identities, which are of the form `(pk, sk, plot)`, where `pk` and `sk` are a cryptographic public/secret key pair, and `plot` is the data for a proof of space/storage that depends on the `pk`.
Creating such a triple should be computationally expensive (especially the generation of `plot`), as well as take up large amounts of disk space, so that it is hard to generate identities frequently, and to store many identities at once.
The public key is the outward identity of a user when posting. When posting, the `sk` is used to sign the post, and `plot` is used to provide a short proof that the `pk` is part of a valid triple.
This makes it impossible to post without having a large plot file for each identity to be employed.

**Trip keys**&emsp;
Tripcodes are pairs of `(pk, sk)`, with no artificially inflated storage or generation cost.
The tripcode name is the public key's textual representation (maybe out of some alphabet of visible characters), so to generate a trip name, a matching public key has to be grinded.

**Curators**&emsp;
Curators are volunteers who maintain public lists of participant IDs / trip keys, posts and files to filter.
Everyone can be a curator.

**"Blockchain"**&emsp;
Instead of a traditional blockchain, a forgetful/self-pruning blockchain is used, where any blocks that are older than some limit are forgotten.
This is possible because blockchains are transient in nature, and no immutable ledger all the way back to the genesis of the image board is needed, and will also help with the goal of a low entry-barrier, as the storage capacity requirements of the system will not infinitely with time.
No content is stored on the blockchain, only meta-data required to operate the network, which consists of the spam protection difficulties for DMs, Posts, and Threads.
In regular intervals, the network decides on new spam protection difficulties using a proof of work or proof of storage/space.
These intervals should be reasonably short to allow the system to adapt to spiking levels of traffic (maybe a few minutes or something?).
The blockchain has to be at least as long as the maximum existence duration for threads and posts.
>It would be perfect if we could remove the blockchain altogether and try to determine the spam protection difficulty locally, and have some kind of coherent swarm behaviour arising from this.

**Posts/threads**&emsp;
Threads are special posts (OPs) that are queryable on the board-level.
Every other post belongs to a single thread, which is hash-referenced as its OP.
All posts and threads have to contain the block height (i.e., index of the block since genesis) of the blockchain at the time of their creation, as well as a recent hash from the blockchain, in addition to the spam protection proof of work/space.
This prevents grinding threads ahead of time and then publishing them all at once.
However, the referenced block hash must be old enough so that the possibility of blockchain forks past that point is low.
Threads should probably be much more expensive to create than usual posts.
>We have to find a trade-off between security against forks, which would be able to make existing threads invalid for referencing an invalid hash, and security against precomputation attacks where lots of threads are generated ahead of time and them spammed at once.
>The spam protection difficulty for creating threads also plays into this equation, so it's a 3-way trade-off.

**Content storage/worldview**&emsp;
Just like on previous image boards, the system's content is transient, and is discarded by individual nodes after some time (i.e., a week or so).
Every node has its own subjective view of the network, and no single node has to know the whole state of the network at any time.
As hinted at in the blockchain section, there is no content stored on the blockchain, and the state of the network is obtained by merging (set unification) of the states of all nodes.

**Synchronisation process**&emsp;
When joining the network, first, a new node has to synchronise with the blockchain, and find out the current spam protection difficulties.
Then, the new node requests the OPs of all threads of the board it wants to look at from the network.
If the user wants to look at a thread, he requests all other posts belonging to that thread from the network.
Since every node only has a partial, subjective view of the network, the views of multiple nodes have to be requested and merged: some views may contain posts that others don't contain, and the merged view contains all posts of all received views, as long as they fulfil the spam protection that was active during their time of posting.
The spam protection difficulty of any point in the recent past (as long back as the system can remember) are known since they are on the blockchain.

**Relaying of posts/files**&emsp;
When a valid post is sent into the network, it is relayed by nodes until the whole network is saturated.
However, only the file's meta data and text content are part of the initial "post" network message, and attached files have to be manually requested by each user after having received the post.
This helps reduce bandwidth for mobile users and also prevents illegal files from being spread automatically.
When files or the posts containing them get filtered, they will be deleted.
Downloaded files will not automatically be relayed to other users requesting them, until the user greenlights them for relaying.
This makes it impossible to accidentally spread illegal files one received and also reduces the user's bandwidth usage.
Of course, there can also be an option in the client application to automatically request all files of all posts it receives.
There can also be an option in the client application to automatically relay all downloaded files that are not filtered.
This option is more risky if you're worried about automatically spreading illegal content, though, but for those who don't care about it, it should be added, as it requires less interaction and will increase the availability of the files and the bandwidth at which they can be served.
In the end, everyone is a sovereign individual and has to make their own choices.
The default configuration is the safest, legally.

## Features

**Anonymous posting**&emsp;
Users can generate a user ID and use it to post on the network without any further authentication.
The only requirements for posting are that the ID is generated properly and that posts fulfil the spam protection difficulty.

**Direct messages**&emsp;
The direct messaging feature allows users to communicate with other users even if they do not have a direct connection, by using the network as a relay.
Such a message is expensive for the network, since it needs to be broadcast to everyone, since the location of the recipient in the network topology is not known a priori.
Replying to such a message is much cheaper, since the shortest path the message travelled can be backtraced:
the original recipient sends the reply only to the peer he first received the original message from, and tags the response with the hash of the original message.
The intermediates then look at who they first received the original message (identified by the hash) from, and relay it that way.
This goes all the way until the original sender is reached.
If the transmission fails, the answer has to be broadcast conventionally again.

There are two properties of the message delivery that can be selected:

* Whether the sender is anonymous to the receiver,
* Whether the receiver is anonymous to the relayers.

To send a message where the recipient is visible to relayers, any user can send an encrypted message of the form `(pkD, Enc(pkD; (pkS, msg)), Sig(skS; msg))`, where `pkD` is the recipient's public key, and `pkS`/`skS` is the recipient's ID or trip key pair.
To keep the recipient hidden, a more private message of the form `Enc(pkD; (pkS, H(msg), msg))` can be sent, and the recipient has to try to decrypt it with his keys and check whether the hash field of the decrypted message matches the hash of the decrypted message.
In that case, the message has been recovered.
Otherwise, the decrypted data is completely random and unintelligible.
To keep the sender hidden from the receiver, simply do not sign the message and do not send the sender's public key.

**Direct connection establishment**&emsp;
The direct messaging protocol can be used to create direct P2P connections by sending one's IP:port or TOR hidden service name in a DM to whoever one wants to establish a direct connection with.
Other applications can build upon this feature and use the P2P network as a base for establishing direct connections.

**Moderation of posts**&emsp;
Every user can locally choose to filter single posts or all posts made using a specific ID or trip key, and additionally, can also filter individual files by file hash.
Additionally, users can optionally subscribe to curators, who will send them filter lists to automatically filter content.
Filtered posts can still be seen, but their content is not displayed (unless specified otherwise in the settings?).
This style of moderation is completely decentralised and without any trusted authorities electing people into power, or any users having any say over what other users can or cannot see.

## Requirements

This section contains the requirements this system must fulfil, or, the assumptions under which the system must be able to function.

**Node lifetime**&emsp;
Nodes are not assumed to operate the network for extended periods of time.
It is not assumed that a large share of nodes will ever reconnect to the network after leaving.
The network topology must be able to adapt to frequent changes due to new nodes joining or old nodes leaving.

**Trust**&emsp;
Every participant has to assume that most other participant are not trustworthy or operated by unique parties.
It has to be assumed that there exist some honest parties in the network who produce legit content.
Especially the consensus power for the blockchain is assumed to possibly be mostly malicious due to the large amount of resources attackers can have.

**Cryptography**&emsp;
No single cryptographic scheme employed in the system may considered absolutely safe.
That's why users can choose different encryption, and signature schemes.
This limits the damage caused by a successful attack on a scheme.

**Modularity, maintainability**&emsp;
Every major part of the system has to be easily replaceable or modifiable.
The code has to be split into different modules who interact with each other only through interfaces.
To change a component, only the implementation of some interface has to be changed.

**Subjectivity**&emsp;
All nodes are assumed to only know a partial view of the system.
New nodes have to build their own view from multiple nodes' subjective views by merging them.